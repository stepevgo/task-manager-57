package ru.t1.stepanishchev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;
import ru.t1.stepanishchev.tm.dto.request.ProjectListRequest;
import ru.t1.stepanishchev.tm.dto.response.ProjectListResponse;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;
import ru.t1.stepanishchev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    private final static String NAME = "project-list";

    @NotNull
    private final static String DESCRIPTION = "Show list projects.";

    @Override
    @EventListener(condition = "@projectListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken(), sort);
        @NotNull final ProjectListResponse response = projectEndpoint.listProject(request);
        @NotNull final List<ProjectDTO> projects = response.getProjects();
        if (projects == null) throw new ProjectNotFoundException();
        @NotNull final StringBuilder stringBuilder = new StringBuilder();
        int index = 1;
        for (@Nullable final ProjectDTO project: projects) {
            if (project == null) continue;
            stringBuilder.append(index + ".");
            stringBuilder.append(project.getName() + " : ");
            stringBuilder.append(project.getDescription()  + " : ");
            stringBuilder.append(project.getId());
            System.out.println(stringBuilder);
            index++;
            stringBuilder.setLength(0);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}