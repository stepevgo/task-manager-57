package ru.t1.stepanishchev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIdListener extends AbstractProjectListener {

    @NotNull
    private final static String NAME = "project-completed-by-id";

    @NotNull
    private final static String DESCRIPTION = "Complete project by id.";

    @Override
    @EventListener(condition = "@projectCompleteByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request =
                new ProjectChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}