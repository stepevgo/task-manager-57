package ru.t1.stepanishchev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.endpoint.*;
import ru.t1.stepanishchev.tm.api.service.*;
import ru.t1.stepanishchev.tm.listener.AbstractListener;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;
import ru.t1.stepanishchev.tm.util.SystemUtil;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Nullable
    @Autowired
    private AbstractListener[]  abstractCommands;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    private void prepareStartup() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initPID();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER HAS GONE **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        publisher.publishEvent(new ConsoleEvent(command));
    }

    public void start(@Nullable final String[] args) {
        prepareStartup();
        runByCommand();
    }

    private void runByCommand() {
        while(!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}