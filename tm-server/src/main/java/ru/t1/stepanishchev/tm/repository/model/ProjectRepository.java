package ru.t1.stepanishchev.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.api.repository.model.IProjectRepository;
import ru.t1.stepanishchev.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("FROM Project", Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId, @NotNull String sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.userId = :userId ORDER BY :sort";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM Project";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM Project m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}