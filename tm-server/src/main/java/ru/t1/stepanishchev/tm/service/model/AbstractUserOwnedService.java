package ru.t1.stepanishchev.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.model.IUserOwnedService;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;
import ru.t1.stepanishchev.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R>
        implements IUserOwnedService<M> {

}