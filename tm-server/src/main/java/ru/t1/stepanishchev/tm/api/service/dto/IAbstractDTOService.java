package ru.t1.stepanishchev.tm.api.service.dto;

import ru.t1.stepanishchev.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.stepanishchev.tm.dto.model.AbstractModelDTO;

public interface IAbstractDTOService<M extends AbstractModelDTO> extends IAbstractDTORepository<M> {
}