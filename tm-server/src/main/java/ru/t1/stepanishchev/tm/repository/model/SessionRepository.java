package ru.t1.stepanishchev.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.api.repository.model.ISessionRepository;
import ru.t1.stepanishchev.tm.model.Session;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull String id) {
        if (id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM Session m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}