package ru.t1.stepanishchev.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.repository.model.ISessionRepository;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.dto.ISessionDTOService;
import ru.t1.stepanishchev.tm.api.service.model.ISessionService;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;
import ru.t1.stepanishchev.tm.exception.entity.SessionNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.IdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.UserIdEmptyException;
import ru.t1.stepanishchev.tm.exception.user.AccessDeniedException;
import ru.t1.stepanishchev.tm.model.Session;
import ru.t1.stepanishchev.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@Service
@NoArgsConstructor
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    @NotNull
    @Override
    public SessionRepository getRepository() {
        return context.getBean(SessionRepository.class);
    }

    @Override
    @NotNull
    public Session create(@Nullable final Session session) {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = getRepository();
        try {
            entityManager.getTransaction().begin();
            repository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

    @Override
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = getRepository();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = getRepository();
        try {
            @Nullable final Session session = repository.findOneById(id);
            return session;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public Session removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = getRepository();
        try {
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

}